*** Settings ***
Documentation     Playground where I will learn to integrate _RobotFramework_ with _SikuliX_
Library           OperatingSystem

*** Variables ***
${BRR6_HOME}      C:\MRTS\CSRR\I1V3\1.4.3.0\BRR6_MAINTENANCE    # Unity Trainer home directory

*** Test Cases ***
Is Simulation Open
    [Documentation]    A quick check to see if the simulation has been opened successfully
    [Setup]    Start brr6
    Log    About to check if the simulation is running

*** Keywords ***
Start brr6
    #launch BRR6
    Log    Beginning BRR6 simulation
    Run    ${BRR6_HOME}//BRR6_Maintenance_Trainer.exe
