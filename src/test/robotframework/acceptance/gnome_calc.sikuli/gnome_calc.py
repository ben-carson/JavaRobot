from sikuli.Sikuli import *

app_launcher = "applauncher-1.png"
search_field = "search_field.png"
calc_launcher = "gnome_calc_launcher.png"
calc_app = "gnome_calc.png"
calc_result = "calc_result.png"

def open_calc():
    if( exists(app_launcher) is not None):
        click(app_launcher)
        wait(search_field)
        click(calc_launcher)
        wait(calc_app,3)
    else:
        print "ERROR: could not locate app launcher. Test Fails!"
    

def quick_add():
    if( exists(calc_app) is not None):
        #click '2'
        click(Pattern(calc_app).offset(-82,121))
        #click '+'
        click(Pattern(calc_app).offset(24,165))
        #click '2'
        click(Pattern(calc_app).offset(-82,121))
        #click '='
        click(Pattern(calc_app).offset(101,165))
    else:
        print "ERROR: could not locate calculator application. TEST FAILS!"

    if( exists(calc_result) is not None):
        print "SUCCESS: result is correct"
    else:
        print "FAIL: result is incorrect. TEST FAILS!"

open_calc()
quick_add()