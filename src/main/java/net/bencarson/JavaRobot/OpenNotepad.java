package net.bencarson.JavaRobot;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class OpenNotepad {

	private static final String imagePath = new String("src\\main\\resources\\images");
	
	public static void main(String[] args) {
		Screen screen = new Screen();
		Pattern pattern = new Pattern(imagePath+"\\NPP.PNG");
		if(!pattern.isValid()) {
			System.err.println("Invalid image");
		} else {
			try {
				screen.click(pattern);
			} catch (FindFailed e) {
				e.printStackTrace();
			}
		}
		
		Pattern nppFileMenu = new Pattern(imagePath+"\\nppFileMenu.PNG").targetOffset(-25, -20);
		Pattern nppFileMenuOpen = new Pattern(imagePath+"\\nppFileMenuOpen.png");
		Pattern nppFileMenuExit = new Pattern(imagePath+"\\nppFileMenuExit.png");
		
		if((screen.exists(nppFileMenu, 2)).isValid()) {
			//Notepad++ has opened
			try {
				//click the File Menu
				screen.click(nppFileMenu);
				//wait for the file menu to display
				screen.wait(nppFileMenuOpen,2);
				//close npp
				screen.click(nppFileMenuExit);
			} catch (FindFailed e) {
				e.printStackTrace();
			}
		}
	}

}
