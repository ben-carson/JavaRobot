package net.bencarson.JavaRobot;

import org.sikuli.script.*;

import net.bencarson.JavaRobot.apps.PuttyApplication;

public class OpenPutty {

	
	public static void main(String[] args) {
		PuttyApplication puttyApp = new PuttyApplication("PuTTY Configuration");
		PuttyApplication.openPutty();
		
		Screen screen = new Screen();
		
		if(!puttyApp.getIconPattern().isValid()) {
			System.err.println("Invalid image");
		} else {
			try {
				puttyApp.focus();
//				Match myMatch = screen.find(puttyApp.getPuttyPattern());
				Match myMatch = screen.exists(puttyApp.getPuttyPattern());
				if(myMatch.isValid()) {
					myMatch.highlight();
				} else {
					System.err.println("This isn't right.");
				}
				App.pause(2);
				screen.mouseMove(myMatch);
				screen.click(puttyApp.getPuttyPattern());
			} catch (FindFailed e) {
				e.printStackTrace();
			}
		}
		
	}
	
}
