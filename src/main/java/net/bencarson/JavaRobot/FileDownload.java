package net.bencarson.JavaRobot;

import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class FileDownload {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\geckodriver.exe");
		FirefoxOptions capabilities = new FirefoxOptions();
		capabilities.setCapability("marionette", true);
		
		FirefoxDriver ffdriver = new FirefoxDriver(capabilities);
		ffdriver.get("http://seleniumhq.org");
		ffdriver.findElement(By.linkText("Download")).click();
		ffdriver.findElement(By.linkText("3.8.1")).click();
		Screen screen = new Screen();
		Pattern pattern = new Pattern("src\\main\\resources\\images\\KeePassIcon.PNG");
		try {
			screen.click(pattern);
		} catch (FindFailed e) {
			e.printStackTrace();
		}
	}
	
}