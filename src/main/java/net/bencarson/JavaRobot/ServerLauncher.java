package net.bencarson.JavaRobot;

import org.robotframework.remoteserver.RemoteServer;

public class ServerLauncher {

	public static void main(String[] args) throws Exception {
		RemoteServer.configureLogging();
		RemoteServer server = new RemoteServer();
		server.setPort(8270);
		server.start();
	}
	
}
