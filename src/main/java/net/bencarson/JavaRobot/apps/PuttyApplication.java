package net.bencarson.JavaRobot.apps;

import org.sikuli.script.App;
import org.sikuli.script.Pattern;

public class PuttyApplication extends App {

	public PuttyApplication(String name) {
		super(name);
	}

	private static final String imagePath = new String("src\\main\\resources\\images");
	private Pattern iconPattern = new Pattern(imagePath + "\\PuttyIcon.PNG");
	private Pattern puttyPattern = new Pattern(imagePath + "\\PuttyOpen.PNG");

	// Accessors
	public Pattern getIconPattern() {
		return iconPattern;
	}

	public void setIconPattern(Pattern iconPattern) {
		this.iconPattern = iconPattern;
	}

	public Pattern getPuttyPattern() {
		return puttyPattern;
	}

	public void setPuttyPattern(Pattern puttyPattern) {
		this.puttyPattern = puttyPattern;
	}

	public static String getImagepath() {
		return imagePath;
	}

	//Methods
	public static void openPutty() {
//		App.open("C:\\Program Files\\PuTTY\\putty.exe");
		App.open("G:\\dev\\tools\\putty\\PUTTY.exe");
	}
	
	//public 
	
}
